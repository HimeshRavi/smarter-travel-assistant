<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="UTF-8">
	<title>Contact</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
	</head>
<body>
	<div id="background">
		<div id="page">
			<div id="header">
				<div id="logo">
					<a href="index.html"><img src="images/logo1.bmp" alt="LOGO" height="112" width="219"></a>
				</div>
				<div id="navigation">
					<ul>
						<li>
							<a href="index.html">Home</a>
						</li>
						<li>
							<a href="attraction.html">Attractions</a>
						</li>
						<li>
						  <a href="nightLife.html">NightLife</a>
						</li>
						<li>
						  <a href="foods.html">Food</a>
						</li>
					  <li>
							<a href="living.html">Living</a>
					  </li>
                      <li>
							<a href="services.html">Services</a>
					  </li>
						<li class="selected">
							<a href="contact.php">Contact</a>
						</li>
					</ul>
				</div>
			</div>
			<div id="contents">
				<div class="box">
					<div>
						<div id="contact" class="body">
						  <h1><u>Contact Us</u>:</h1>
							<div id="apDiv1">
                            <fieldset>
                            <legend><b><u>Feedback Form</u></b></legend>
								<form action = "contact.php" method= "post">
                                Name: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="name"><br>
                                <br>
                                E-mail: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="email" name="email"><br>
                                <br>
                                Telephone: <input type="tel" name="phone"><br>
                                <br>
                                Subject: &nbsp;&nbsp;&nbsp;&nbsp;<input type="textbox" name="subject"><br>
                                <br>
                                Message: &nbsp;&nbsp;<input type="textbox" name="message"><br>
                                <br>
                                <input type="submit" name = "add">
                                </form>
						  </fieldset>
                          <?php
		// upon user click submit button
	if ( isset($_REQUEST['add']) ) 
	{
		// step 1: connect to database server 
		$connection = mysql_connect("localhost", "root", "");
		if ( !$connection ) {
			die('Could not connect to localhost.');	
		}
		
		// step 2: select a database
		$db = mysql_select_db("smart_travel", $connection);
		if ( !$db) {
			die ('Could not find database smart_travel');	
		}

		// step 3: send SQl statement to database
		// construct a insert query
		$insert_query = "INSERT INTO feedback VALUES ('$_POST[name]', '$_POST[email]', '$_POST[phone]', '$_POST[subject]', '$_POST[message]')";
		// echo $insert_query;   //for debug
		
		// run the query and process result
		$result = mysql_query($insert_query, $connection);	
		if ($result == TRUE)
			echo "<h3 style='color:blue;'>New record has been added to the database.</h3>";
		else 
			echo "<h3 style='color:red'>Failed to add new record  to the database.</h3>";
			
		// step 4: close the connection
		mysql_close($connection);		
	}
	
	?>
                            </div>
							
							<h2 align="center">&quot;<u>Smarter Travel Assistant</u>&quot;</h2>
							<p>
								<span><b><u>Address</u></b>:</span> 15 Mandarina Avenue, Marina Square,<br>
								Singapore 039799
						  </p>
							<p>
								<span><b><u>Telephone Number</u></b>:</span> +65 6338 0880
							</p>
							<p>
								<span><b><u>Fax Number</u></b>:</span> +65 6338 0889
							</p>
                            <p>
								<span><b><u>Email</u></b>:</span><a href="mailto:your@mail.com" target="_blank">sta@gmail.com</a>
							</p>
					  </div>
					</div>
				</div>
			</div>
		</div>
		<div id="footer">
			<div>
				<ul class="navigation">
					<li>
						<a href="index.html">Home</a>
					</li>
					<li>
						<a href="attraction.html">Attractions</a>
					</li>
					<li>
						<a href="nightLife.html">NightLife</a>
					</li>
					<li>
						<a href="foods.html">Food</a>
					</li>
					<li>
						<a href="living.html">Living</a>
					</li>
					<li>
						<a href="services.html">Services</a>
					</li>
					<li class="active">
						<a href="contact.php">Contact</a>
					</li>
				</ul>
				<div id="connect">
					<a href="http://www.facebook.com/" target="_blank" class="facebook"></a>
                    <a href="http://www.twitter.com/" target="_blank" class="twitter"></a>
                    <a href="http://plus.google.com/" target="_blank" class="googleplus"></a>
				</div>
			</div>
			<p>
				© 2014 by <a href="index.html">SMARTER TRAVEL ASSISTANT</a>. All Rights Reserved
			</p>
		</div>
	</div>
</body>
</html>